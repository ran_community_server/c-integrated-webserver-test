﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Windows.Forms;
using HttpServer;

namespace IntegratedWebserverTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private bool webserverRunning = false;

        private HttpServer.HttpListener listener;

        private void button1_Click(object sender, EventArgs e)
        {
            if (webserverRunning)
            {
                webserverRunning = false;
                listener.Stop();
                numericUpDown1.Enabled = true;
                button1.Text = "Webserver starten";
            }
            else
            {
                webserverRunning = true;
                numericUpDown1.Enabled = false;
                button1.Text = "Webserver stoppen";
                listener = HttpServer.HttpListener.Create(System.Net.IPAddress.Any, (int)numericUpDown1.Value);
                listener.RequestReceived += OnRequest;
                listener.Start(5);
            }
        }

        private void OnRequest(object sender, RequestEventArgs e)
        {
            IHttpClientContext context = (IHttpClientContext)sender;
            IHttpRequest request = e.Request;
            IHttpResponse response = request.CreateResponse(context);
            StreamWriter writer = new StreamWriter(response.Body);
            writer.WriteLine("<html><body>Hello wordl!</body></html>");
            writer.Flush();
            response.Send();
            AddStringToListbox("HTTP Request: " + e.Request.Method + " " + e.Request.Uri);
        }

        private bool webserverRunningSSL = false;

        private HttpServer.HttpListener listenerSSL;

        private void button2_Click(object sender, EventArgs e)
        {
            if (webserverRunningSSL)
            {
                webserverRunningSSL = false;
                listenerSSL.Stop();
                numericUpDown2.Enabled = true;
                button2.Text = "Webserver starten";
            }
            else
            {
                webserverRunningSSL = true;
                numericUpDown2.Enabled = false;
                button2.Text = "Webserver stoppen";
                var certificate = new X509Certificate2("NADINE.cer");
                listenerSSL = HttpServer.HttpListener.Create(System.Net.IPAddress.Any, (int)numericUpDown2.Value, certificate);
                listenerSSL.RequestReceived += OnRequestSSL;
                listenerSSL.Start(5);
            }
        }

        private void OnRequestSSL(object sender, RequestEventArgs e)
        {
            IHttpClientContext context = (IHttpClientContext)sender;
            IHttpRequest request = e.Request;
            IHttpResponse response = request.CreateResponse(context);
            StreamWriter writer = new StreamWriter(response.Body);
            writer.WriteLine("<html><body>Hello secure world!</body></html>");
            writer.Flush();
            response.Send();
            AddStringToListbox("HTTPS Request: " + e.Request.Method + " " + e.Request.Uri);
        }

        private delegate void StringMethod(string str);

        private void AddStringToListbox(string str)
        {
            if (listBox1.InvokeRequired)
            {
                listBox1.Invoke(new StringMethod(AddStringToListbox), new object[] {str});
                return;
            }
            listBox1.Items.Add(str);
        }
    }
}
